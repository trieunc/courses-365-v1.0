"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";

var gCourseId = "";

const gTABLE_ROW = ["id", "coverImage", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "teacherName", "teacherPhoto", "isPopular", "isTrending"];
const gSTT_COL = 0;
const gCOVER_IMG_COL = 1;
const gCODE_COL = 2;
const gNAME_COL = 3;
const gPRICE_COL = 4;
const gDISCOUNT_COL = 5;
const gDURATION_COL = 6;
const gLEVEL_COL = 7;
const gTEACHER_COL = 8;
const gTEACHER_IMG_COL = 9;
const gPOPULAR_COL = 10;
const gTRENDING_COL = 11;
const gACTION_COL = 12;

var gSTT = 1;

var gTableCourses = $("#table-courses").DataTable({
    columns: [
        {data: gTABLE_ROW[gSTT_COL]},
        {data: gTABLE_ROW[gCOVER_IMG_COL]},
        {data: gTABLE_ROW[gCODE_COL]},
        {data: gTABLE_ROW[gNAME_COL]},
        {data: gTABLE_ROW[gPRICE_COL]},
        {data: gTABLE_ROW[gDISCOUNT_COL]},
        {data: gTABLE_ROW[gDURATION_COL]},
        {data: gTABLE_ROW[gLEVEL_COL]},
        {data: gTABLE_ROW[gTEACHER_COL]},
        {data: gTABLE_ROW[gTEACHER_IMG_COL]},
        {data: gTABLE_ROW[gPOPULAR_COL]},
        {data: gTABLE_ROW[gTRENDING_COL]},
        {data: gTABLE_ROW[gACTION_COL]},
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            render: function() {
                return gSTT++;
            }
        },
        {
            targets: gCOVER_IMG_COL,
            render: function(value) {
                return `<img src="${value}" class="rounded" style="width: 160px">`;
            }
        },
        {
            targets: [gPRICE_COL, gDISCOUNT_COL],
            render: function(value) {
                return "$" + value;
            }
        },
        {
            targets: gTEACHER_COL,
            className: "text-nowrap"
        },
        {
            targets: gTEACHER_IMG_COL,
            render: function(value) {
                return `<img src="${value}" class="rounded" style="width: 80px">`;
            }
        },
        {
            targets: [gPOPULAR_COL, gTRENDING_COL],
            render: function(value) {
                if (String(value) == "true") {
                    return `<i class="fa-regular fa-square-check fa-xl"></i>`
                }
                else if (String(value) == "false") {
                    return `<i class="fa-regular fa-square fa-xl"></i>`
                }
            }
        },
        {
            targets: gACTION_COL,
            defaultContent: 
            `
                <i class="fa-solid fa-lg fa-pen-to-square text-success update-course" title="Update Course" role="button"></i>
                <i class="fa-solid fa-lg fa-trash-can text-danger ml-2 delete-course"title="Delete Course" role="button"></i>
            `
        }
    ]
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện load trang/F5
$(document).ready(onPageLoading);

// gán sự kiện click button Create New Course
$(document).on("click", ".new-course", onBtnNewCourseClick);
// gán sự kiện click button New Course trên Modal Create Course
$(document).on("click", ".btn-create", onBtnModalNewCourseClick);
// gán sự kiện click button Clear Form trên Modal Create Course
$(document).on("click", ".btn-clear", onBtnModalClearClick);

// gán sự kiện click button Update Course
$(document).on("click", ".update-course", onBtnUpdateCourseClick);
// gán sự kiện click button Update Course trên Modal Update Course
$(document).on("click", ".btn-update", onBtnModalUpdateCourseClick);

// gán sự kiện click button Delete Course
$(document).on("click", ".delete-course", onBtnDeleteCourseClick);
// gán sự kiện click button confirm Delete Course trên Modal Delete course
$(document).on("click", ".btn-delete", onBtnModalDeleteCourseClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sự kiện load trang/F5
function onPageLoading() {
    // Gọi API lấy danh sách courses
    let vAjaxRequest = apiGetAllCourses();
    vAjaxRequest.done(function(courses) {
        // Xử lý hiển thị
        console.log(courses)
        loadCoursesDataToTable(courses);
        // căn giữa hàng table data
        modifyTableDataForm();
    });
}

// function xử lý sự kiện click button Create New Course
function onBtnNewCourseClick() {
    // hiển thị modal điền thông tin khóa học mới
    $("#modal-create-course").modal("show");
}

// function xử lý sự kiện click button Clear Form trên Modal Create Course
function onBtnModalClearClick() {
    clearModalCreateCourseForm();
}

// function xử lý sự kiện click button New Course trên Modal Create Course
function onBtnModalNewCourseClick() {
    // Khai báo đối tượng chứa dữ liệu
    let vCourseDetail = {
        courseCode: "",
        courseName: "",
        price: "",
        discountPrice: "",
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // Thu thập dữ liệu
    getCourseDetail(vCourseDetail);
    // Kiểm tra dữ liệu
    let vDataValid = checkCourseDetail(vCourseDetail);
    if (vDataValid) {
        $("#modal-create-course").modal("hide");
        // Gọi API tạo khóa học mới
        let vAjaxRequest = apiCreateCourse(vCourseDetail);
        vAjaxRequest.done(function() {
            alert("Create New Course Success");
            // xóa dữ liệu trên form
            clearModalCreateCourseForm();
            // load lại table course
            onPageLoading();
        })
    }
}

// function xử lý sự kiện click button Update Course
function onBtnUpdateCourseClick() {
    // hiển thị modal update thông tin khóa học
    $("#modal-update-course").modal("show");
    // lấy giá trị course ID lưu vào biến toàn cục gCourseId
    getCourseId(this);
    // Gọi API lấy thông tin khóa học bằng ID
    let vAjaxRequest = apiGetCourseDetailById();
    vAjaxRequest.done(function(courseDetail) {
        // Hiển thị thông tin khóa học lên form Modal Update Course
        displayCourseDetail(courseDetail);
    })
}

// function xử lý sự kiện click button Update Course trên Modal Update Course
function onBtnModalUpdateCourseClick() {
    // Khai báo đối tượng chứa dữ liệu
    let vCourseDetail = {
        courseCode: "",
        courseName: "",
        price: "",
        discountPrice: "",
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // Thu thập dữ liệu
    getUpdateCourseDetail(vCourseDetail);
    // Kiểm tra dữ liệu
    let vDataValid = checkCourseDetail(vCourseDetail);
    if (vDataValid) {
        $("#modal-update-course").modal("hide");
        // Gọi API tạo khóa học mới
        let vAjaxRequest = apiUpdateCourseDetailById(vCourseDetail);
        vAjaxRequest.done(function() {
            alert("Update Course Success");
            // load lại table course
            onPageLoading();
        })
    }
}

// function xử lý sự kiện click button Delete Course
function onBtnDeleteCourseClick() {
    // hiển thị modal Delete khóa học
    $("#modal-delete-course").modal("show");
    // lấy giá trị course ID lưu vào biến toàn cục gCourseId
    getCourseId(this);
    console.log(gCourseId);
}

// function xử lý sự kiện click button Delete Course trên Modal Delete course
function onBtnModalDeleteCourseClick() {
    $("#modal-delete-course").modal("hide");
    // gọi API xóa course theo ID
    let vAjaxRequest = apiDeleteCourseById();
    vAjaxRequest.done(function() {
        alert("Delete Course successful");
        // Tải lại table courses
        onPageLoading();
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function gọi API lấy danh sách courses (all courses)
function apiGetAllCourses() {
    return $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
    });
}

// function gọi API tạo mới khóa học (Create)
function apiCreateCourse(paramCourseDetail) {
    let vStringData = JSON.stringify(paramCourseDetail);
    return $.ajax({
        url: gBASE_URL + "/courses",
        type: "POST",
        data: vStringData,
        contentType: "application/json",
    })
}

// function gọi API lấy thông tin khóa học bằng ID
function apiGetCourseDetailById() {
    return $.ajax({
        url: gBASE_URL + "/courses/" + gCourseId,
        type: "GET",
    });
}

// function gọi API update thông tin khóa học bằng ID
function apiUpdateCourseDetailById(paramCourseDetail) {
    let vStringData = JSON.stringify(paramCourseDetail);
    return $.ajax({
        url: gBASE_URL + "/courses/" + gCourseId,
        type: "PUT",
        data: vStringData,
        contentType: "application/json",
    });
}

// function gọi API Delete khóa học theo course ID (Delete)
function apiDeleteCourseById() {
    return $.ajax({
        url: gBASE_URL + "/courses/" + gCourseId,
        type: "DELETE",
    });
}

// function load dữ liệu courses vào data table
function loadCoursesDataToTable(paramCourses) {
    gSTT = 1;
    gTableCourses.clear();
    gTableCourses.rows.add(paramCourses);
    gTableCourses.draw()
}

// function get course ID cho button Update và Delete
function getCourseId(paramButton) {
    let vRowClick = $(paramButton).closest("tr");
    let vCourseData = gTableCourses.row(vRowClick).data();
    gCourseId = vCourseData.id;
}

// function hiển thị course detail lên form trên modal Update Course
function displayCourseDetail(paramCourseDetail) {
    $("#update-course-code").val(paramCourseDetail.courseCode);
    $("#update-course-name").val(paramCourseDetail.courseName);
    $("#update-course-price").val(paramCourseDetail.price);
    $("#update-course-discount").val(paramCourseDetail.discountPrice);
    $("#update-course-duration").val(paramCourseDetail.duration);
    $("#update-course-level").val(paramCourseDetail.level);
    $("#update-course-cover-img").val(paramCourseDetail.coverImage);
    $("#update-course-teacher").val(paramCourseDetail.teacherName);
    $("#update-course-teacher-img").val(paramCourseDetail.teacherPhoto);
    $("#update-course-popular").val(paramCourseDetail.isPopular);
    $("#update-course-trending").val(paramCourseDetail.isTrending);
}

// function clear form trên modal Creat Course
function clearModalCreateCourseForm() {
    $("#new-course-code").val("");
    $("#new-course-name").val("");
    $("#new-course-price").val("");
    $("#new-course-discount").val("");
    $("#new-course-duration").val("");
    $("#new-course-level").val("");
    $("#new-course-cover-img").val("");
    $("#new-course-teacher").val("");
    $("#new-course-teacher-img").val("");
    $("#new-course-popular").val("");
    $("#new-course-trending").val("");
}

// function thu thập dữ liệu người dùng nhập trên form Modal Create Course
// function trả về đối tượng paramCourseDetail được tham số hóa
function getCourseDetail(paramCourseDetail) {
    paramCourseDetail.courseCode = $("#new-course-code").val().trim();
    paramCourseDetail.courseName = $("#new-course-name").val().trim();
    paramCourseDetail.price = $("#new-course-price").val().trim();
    paramCourseDetail.discountPrice = $("#new-course-discount").val().trim();
    paramCourseDetail.duration = $("#new-course-duration").val().trim();
    paramCourseDetail.level = $("#new-course-level").val().trim();
    paramCourseDetail.coverImage = $("#new-course-cover-img").val().trim();
    paramCourseDetail.teacherName = $("#new-course-teacher").val().trim();
    paramCourseDetail.teacherPhoto = $("#new-course-teacher-img").val().trim();
    paramCourseDetail.isPopular = $("#new-course-popular").val();
    paramCourseDetail.isTrending = $("#new-course-trending").val();
}

// function thu thập dữ liệu người dùng nhập trên form Modal Update Course
// function trả về đối tượng paramCourseDetail được tham số hóa
function getUpdateCourseDetail(paramCourseDetail) {
    paramCourseDetail.courseCode = $("#update-course-code").val().trim();
    paramCourseDetail.courseName = $("#update-course-name").val().trim();
    paramCourseDetail.price = $("#update-course-price").val().trim();
    paramCourseDetail.discountPrice = $("#update-course-discount").val().trim();
    paramCourseDetail.duration = $("#update-course-duration").val().trim();
    paramCourseDetail.level = $("#update-course-level").val().trim();
    paramCourseDetail.coverImage = $("#update-course-cover-img").val().trim();
    paramCourseDetail.teacherName = $("#update-course-teacher").val().trim();
    paramCourseDetail.teacherPhoto = $("#update-course-teacher-img").val().trim();
    paramCourseDetail.isPopular = String($("#update-course-popular").val().trim());
    paramCourseDetail.isTrending = String($("#update-course-trending").val().trim());
}



// function kiểm tra dữ liệu nhập trên form
// function return true nếu tất cả dữ liệu hợp lệ, return false nếu có dữ liệu không hợp lệ
function checkCourseDetail(paramCourseDetail) {
    if (paramCourseDetail.courseCode == "") {
        alert("Please enter Course Code");
        return false;
    }
    else if (paramCourseDetail.courseName == "") {
        alert("Please enter Course Name");
        return false;
    }
    else if (paramCourseDetail.price == "") {
        alert("Please enter Course Price");
        return false;
    }
    else if (paramCourseDetail.discountPrice == "") {
        alert("Please enter Course Discount Price");
        return false;
    }
    else if (paramCourseDetail.duration == "") {
        alert("Please enter Course Duration");
        return false;
    }
    else if (paramCourseDetail.level == "") {
        alert("Please enter Course Level");
        return false;
    }
    else if (paramCourseDetail.coverImage == "") {
        alert("Please enter Course Cover Image Url");
        return false;
    }
    else if (paramCourseDetail.teacherName == "") {
        alert("Please enter Teacher Name");
        return false;
    }
    else if (paramCourseDetail.teacherPhoto == "") {
        alert("Please enter Teacher Photo Url");
        return false;
    }
    else if (paramCourseDetail.isPopular == "") {
        alert("Please enter Course Popular (true/false)");
        return false;
    }
    else if (paramCourseDetail.isTrending == "") {
        alert("Please enter Course Trending (true/false)");
        return false;
    }
    return true;
}

// function hiệu chỉnh column của table
function modifyTableDataForm() {
    $("#table-courses td,th").addClass("align-middle");
}